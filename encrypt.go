package main

import (
	"fmt"
	"strconv"
	"os"
	"math/rand"
	"time"
	"io/ioutil"
	"github.com/schollz/progressbar/v3"
)

//Initialize random seed
func init() {
    rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

//Function to generate random key
func RandStringRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func main() {
	//This variable contains an array of bytes.
	//Any file is an array of bytes
	//So the encrypted file will be saved in this variable
	var result []byte

	//First, we get the name of the file to encrypt (first argument)
	exec := os.Args[1]
	steps := 1
	if len(os.Args) > 2 {
		stepsInt, err := strconv.Atoi(os.Args[2])
		if err != nil {
			fmt.Println(err)
			return
		}
		steps = stepsInt
	}
	//Then, we generate a random key of 15 characters
	key := RandStringRunes(15)
	fmt.Println("Encoding", exec,"content with key", key,"and step of", steps)
	//Reading the clear file content
	b, err := ioutil.ReadFile(exec) // just pass the file name
	if err != nil {
		fmt.Println(err)
	}
	//Adding the key to the result, so the resulted file will be KEY+ENCRYPTED_DATA
	//We're doing this to have the key to be able to decrypt the file later
	result = append(result, key...)
	//Adding step to the binary too
	result = append(result, byte(steps))
	result = append(result, b...)
	//Foreach byte of the file, we're merging it with the secret key
	//Exemple, if the key is TEST
	//And if we want to encrypt Salut
	//The process will be like that
	//
	//   S a l u t
	// + T E S T T
	// = K * ¨ / b
	//
	// (This example is not real, random characters
	bar := progressbar.Default(int64(len(b)/steps))
	for i := 0; i < (len(b)/steps); i++ {
		bar.Add(1)
		//Iterating over all bytes of our binary	
		//This little loop is to repeat the key as long as the file is big
		l := i
		for l >= 15 {
			l -= 15
		}
		//We're merging current byte to the matching byte in the key
		result[16+i] = result[16+i] + key[l]
		//NOTE: We're adding 16 to the byte index because 16th first bytes are the key + the step
	}
	//Finally, we're writing result in a new file, its name is the same as the original with .chiffred
	ioutil.WriteFile("./"+exec+".chiffred", result, 444)
    	fmt.Println("DONE")

}
