# Usage
To download all dependencies, a

	go mod tidy

is needed

First, compile encrypt.go


	go build encrypt.go


It will produce an executable _encrypt_ (or _encrypt.exe_ if you're using windows)

Then, place your "malicious" executable in the same directory

Run:

	./encrypt(.exe) myexecutable(.exe)


By default, encrypt will encrypt ALL the bytes of the file. It's VERY *slow*

If you wanna get things going faster, just pass to encrypt the step to use (exemple, if steps is 4, it will only encrypt one byte on four, it's four times faster and not really less secure)

Like this:

	./encrypt(.exe) myexecutable(.exe) 4

(It can be 1,2,50, or whatever you want)

Then, it should produce a file called myexecutable(.exe).chiffred

Edit main.go to replace //go:embed hello.chiffred by //go:embed myexecutable(.exe).chiffred

Then, compile main.go


	go build main.go


It will produce an executable called _main(.exe)_

This executable contains your encrypted binary. Just launch it, and after a while, your encrypted binary will be executed as wanted :)
It supports args, so if your binary need some cli args, just pass them as normal when calling main.go build

